﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Nefarius.ViGEm.Client;
using Nefarius.ViGEm.Client.Targets;
using Nefarius.ViGEm.Client.Targets.Xbox360;
using Sensorit.Base;
using joyconmappertest.JoyConLibrary;

namespace joyconmappertest
{
    public class Mapper
    {
        public enum DesiredMapping : uint
        {
            None,
            Single,
            Joined,
            Mixed_Mouse,
            Mixed_Gyro_Mouse,
            Mixed_Gyro_Mouse_Joystick,
        }

        private const int X360_STICK_MAX = 32767;
        private const int X360_STICK_MIN = -32768;
        private const int OUTPUT_X360_RESOLUTION = X360_STICK_MAX - X360_STICK_MIN;

        private const int HOLD_CURSOR_NUM_POLL = 2;

        private static ushort leftButtonComb = 0;
        private static ushort rightButtonComb = 0;

        private double mouseX = 0.0;
        private double mouseY = 0.0;
        private double mouseXRemainder = 0.0;
        private double mouseYRemainder = 0.0;
        //private OneEuroFilter filterX = new OneEuroFilter(minCutoff: 2.0, beta: 0.2);
        //private OneEuroFilter filterY = new OneEuroFilter(minCutoff: 2.0, beta: 0.2);
        private OneEuroFilter filterX = new OneEuroFilter(minCutoff: 0.3, beta: 0.3); // Gyro Mouse
        private OneEuroFilter filterY = new OneEuroFilter(minCutoff: 0.3, beta: 0.3); // Gyro Mouse
        //private OneEuroFilter filterX = new OneEuroFilter(minCutoff: 0.0008, beta: 0.001); // Attemp Gyro Mouse Joystick
        //private OneEuroFilter filterY = new OneEuroFilter(minCutoff: 0.0008, beta: 0.001); // Attemp Gyro Mouse Joystick
        //private OneEuroFilter filterX = new OneEuroFilter(minCutoff: 10.0, beta: 0.7);
        //private OneEuroFilter filterY = new OneEuroFilter(minCutoff: 10.0, beta: 0.7);
        private double currentRate = 1.0; // Expressed in Hz
        private double currentLatency = 1.0; // Expressed in sec
        private double currentRateJoyR = 1.0; // Expressed in Hz
        private double currentLatencyJoyR = 1.0;  // Expressed in sec

        private bool disableGyro;
        private bool holdCursor;
        private int holdCursorCount;
        private bool mouseLBDown;
        private bool mouseRBDown;

        private Thread vbusThr;
        private Thread contThr;

        private ViGEmClient vigemTestClient = null;
        private IXbox360Controller outputX360 = null;
        private JoyConEnumerator joyconDeviceEnumerator;
        private Dictionary<JoyConDevice, JoyConReader> deviceReadersMap;

        private ReaderWriterLockSlim x360ConReportLocker;

        public Mapper()
        {
            joyconDeviceEnumerator = new JoyConEnumerator();
            deviceReadersMap = new Dictionary<JoyConDevice, JoyConReader>();
        }

        public void Start()
        {
            // Change thread affinity of bus object to not be tied
            // to GUI thread
            vbusThr = new Thread(() =>
            {
                vigemTestClient = new ViGEmClient();
            });

            vbusThr.Priority = ThreadPriority.AboveNormal;
            vbusThr.IsBackground = true;
            vbusThr.Start();
            vbusThr.Join(); // Wait for bus object start

            contThr = new Thread(() =>
            {
                x360ConReportLocker = new ReaderWriterLockSlim();
                outputX360 = vigemTestClient.CreateXbox360Controller();
                outputX360.AutoSubmitReport = false;
                outputX360.Connect();
            });
            contThr.Priority = ThreadPriority.Normal;
            contThr.IsBackground = true;
            contThr.Start();
            contThr.Join(); // Wait for bus object start

            Thread temper = new Thread(() =>
            {
                joyconDeviceEnumerator.FindControllers();
            });
            temper.IsBackground = true;
            temper.Priority = ThreadPriority.Normal;
            temper.Name = "HID Device Opener";
            temper.Start();
            temper.Join();

            PrepareButtonResetCombos(DesiredMapping.Mixed_Gyro_Mouse);

            foreach (JoyConDevice joyCon in joyconDeviceEnumerator.GetDevices())
            {
                JoyConReader reader = new JoyConReader(joyCon);
                deviceReadersMap.Add(joyCon, reader);
                if (joyCon.SideType == JoyConSide.Left)
                {
                    reader.Report += Reader_Left_Mixed_Gyro_Report;
                    //reader.Report += Reader_Left_Join_Report;
                    //reader.Report += Reader_Left_Single_Report;
                }
                else if (joyCon.SideType == JoyConSide.Right)
                {
                    reader.Report += Reader_Calibrate_Gyro;
                    //reader.Report += Reader_Right_Mixed_Gyro_Report;
                    //reader.Report += Reader_Right_Join_Report;
                    //reader.Report += Reader_Right_Single_Report;
                }

                /*outputX360.FeedbackReceived += (sender, e) =>
                {
                    //joyCon.currentLeftAmpRatio = e.LargeMotor / 255.0;
                    //joyCon.currentRightAmpRatio = e.SmallMotor / 255.0;
                    //reader.WriteReport();
                };
                */

                reader.StartUpdate();
            }
        }

        //private bool rightGyroCalibrated = false;
        private const int CALIB_POLL_COUNT = 240; // Roughly 4 seconds of polls
        private int rightGyroCalibPolls = 0;
        private int ignoreGyroCalibPolls = 0;
        private const int TOTAL_IGNORE_CALIB_POLL_COUNT = 67; // Roughly 1 second of polls
        private int gyroYawCalibSum = 0;
        private int gyroPitchCalibSum = 0;
        private int gyroRollCalibSum = 0;

        /// <summary>
        /// Method used to hook the desired static mapping routine after gyro calibration is
        /// finished. Needed for now until a more dynamic solution is implemented.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="device"></param>
        private void HookReaderEvent(JoyConReader sender, JoyConDevice device)
        {
            sender.Report += Reader_Right_Mixed_Gyro_Report;
        }

        private void Reader_Calibrate_Gyro(JoyConReader sender, JoyConDevice device)
        {
            ref JoyConState current = ref device.ClothOff;

            if (ignoreGyroCalibPolls < TOTAL_IGNORE_CALIB_POLL_COUNT)
            {
                ignoreGyroCalibPolls++;
                return;
            }

            if (rightGyroCalibPolls == 0)
            {
                Console.WriteLine("Starting Calib");
            }

            gyroYawCalibSum += current.Motion.GyroYaw;
            gyroPitchCalibSum += current.Motion.GyroPitch;
            gyroRollCalibSum += current.Motion.GyroRoll;

            //rightGyroYawCache[rightGyroCalibPolls] = current.Motion.GyroYaw;
            //rightGyroPitchCache[rightGyroCalibPolls] = current.Motion.GyroPitch;
            //rightGyroRollCache[rightGyroCalibPolls] = current.Motion.GyroRoll;

            rightGyroCalibPolls++;
            if (rightGyroCalibPolls >= CALIB_POLL_COUNT)
            {
                //rightGyroCalibrated = true;

                /*Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");

                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");

                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");
                Console.WriteLine("OBTAINED DESIRED POLL COUNT. CALCULATE OFFSETS.");*/

                short calibOffsetYaw = (short)(gyroYawCalibSum / rightGyroCalibPolls),
                    calibOffsetPitch = (short)(gyroPitchCalibSum / rightGyroCalibPolls),
                    calibOffsetRoll = (short)(gyroRollCalibSum / rightGyroCalibPolls);

                // JoyCon R gyro data gets inverted to make it uniform with JoyCon L data.
                // Need to invert calculated calib offsets to correspond with device raw coordinate system
                if (device.SideType == JoyConSide.Right)
                {
                    calibOffsetYaw *= -1; calibOffsetPitch *= -1; calibOffsetRoll *= -1;
                }

                device.gyroCalibOffsets[JoyConDevice.IMU_YAW_IDX] = calibOffsetYaw;
                device.gyroCalibOffsets[JoyConDevice.IMU_PITCH_IDX] = calibOffsetPitch;
                device.gyroCalibOffsets[JoyConDevice.IMU_ROLL_IDX] = calibOffsetRoll;
                //Console.WriteLine(string.Join(",", device.gyroCalibOffsets));

                sender.Report -= Reader_Calibrate_Gyro;
                HookReaderEvent(sender, device);
                //sender.Report += Reader_Right_Mixed_Gyro_Report;
                Console.WriteLine("CALIB DONE");
            }
        }

        private void Reader_Right_Mixed_Gyro_Report(JoyConReader sender, JoyConDevice device)
        {
            x360ConReportLocker.EnterWriteLock();

            ref JoyConState current = ref device.ClothOff;
            ref JoyConState previous = ref device.ClothOff2;
            //outputX360.ResetReport();
            currentLatencyJoyR = sender.CombLatency;
            currentRateJoyR = 1.0 / currentLatencyJoyR;

            unchecked
            {
                //ushort tempButtons = 0;
                ushort tempButtons = (ushort)(outputX360.ButtonState & ~rightButtonComb);
                if (current.B) tempButtons |= Xbox360Button.A.Value;
                if (current.A) tempButtons |= Xbox360Button.B.Value;
                if (current.Y) tempButtons |= Xbox360Button.X.Value;
                if (current.X) tempButtons |= Xbox360Button.Y.Value;
                if (current.Plus) tempButtons |= Xbox360Button.Start.Value;
                if (current.Home) tempButtons |= Xbox360Button.Guide.Value;
                if (current.RSClick) tempButtons |= Xbox360Button.RightThumb.Value;
                //if (current.RSClick) tempButtons |= Xbox360Button.LeftShoulder.Value;
                if (current.RShoulder) tempButtons |= Xbox360Button.RightShoulder.Value;
                //if (current.SL) tempButtons |= Xbox360Button.LeftShoulder.Value;
                //if (current.SR) tempButtons |= Xbox360Button.RightShoulder.Value;
                //if (current.SR) tempButtons |= Xbox360Button.LeftShoulder.Value;

                outputX360.SetButtonsFull(tempButtons);
            }

            /*short temp;
            temp = AxisScale(current.RX, JoyConReader.STICK_X_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_X, false);
            //outputX360.LeftThumbX = temp;
            outputX360.RightThumbX = temp;

            temp = AxisScale(current.RY, JoyConReader.STICK_Y_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_Y, false);
            //outputX360.LeftThumbY = temp;
            outputX360.RightThumbY = temp;
            */

            //temp = AxisScale(current.RX, JoyConReader.STICK_X_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_X, false);
            //outputX360.RightThumbX = temp;
            //outputX360.RightThumbY = temp;

            //temp = AxisScale(current.RY, JoyConReader.STICK_Y_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_Y, false);
            //temp = AxisScale(current.RY, JoyConReader.STICK_Y_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_Y, true);
            //outputX360.RightThumbY = temp;
            //outputX360.RightThumbX = temp;

            //outputX360.RightTrigger = current.ZR ? (byte)255 : (byte)0;

            // Skip if duration is less than 10 ms
            /*
            if (current.timeElapsed > 0.01)
            //if (currentLatency > 0.01)
            {
                if (!disableGyro)
                {
                    PrepareGyroMouseJoystickEvent(ref current, ref previous);
                }
                else
                {
                    // Take possible lag state into account. Main routine will make sure to skip this method
                    //if (previous.timeElapsed <= 0.002)
                    //{
                    //    double timeElapsed = current.timeElapsed;
                    //    timeElapsed += previous.timeElapsed;
                    //    currentRate = 1.0 / timeElapsed;
                    //}

                    filterX.Filter(0.0, currentRate); // Smooth on output
                    filterY.Filter(0.0, currentRate); // Smooth on output

                    outputX360.RightThumbX = 0;
                    outputX360.RightThumbY = 0;
                }

                sender.CombLatency = 0;
            }*/

            outputX360.RightTrigger = 0;
            outputX360.RightThumbX = 0;
            outputX360.RightThumbY = 0;

            if (current.ZR != previous.ZR)
            {
                mouseLBDown = current.ZR;
                InputMethods.MouseEvent(current.ZR ? InputMethods.MOUSEEVENTF_LEFTDOWN :
                    InputMethods.MOUSEEVENTF_LEFTUP);

                //holdCursor = true;
                holdCursorCount = 0;
            }

            // Skip if duration is less than 10 ms
            bool tempSkipMouse = false;
            //if (current.timeElapsed > 0.01)
            //if (currentLatency > 0.01)
            {
                /*if (holdCursor && holdCursorCount >= HOLD_CURSOR_NUM_POLL)
                {
                    holdCursor = false;
                    holdCursorCount = 0;
                }
                else if (holdCursor)
                {
                    //Console.WriteLine($"IN HERE {holdCursorCount}");
                    holdCursorCount++;
                }
                */

                if (!disableGyro && !holdCursor)
                {
                    PopulateGyroMouseEventData(ref current, ref previous);
                }
                else
                {
                    mouseXRemainder = mouseYRemainder = 0.0;
                    // Take possible lag state into account. Main routine will make sure to skip this method
                    /*if (previous.timeElapsed <= 0.002)
                    {
                        double timeElapsed = current.timeElapsed;
                        timeElapsed += previous.timeElapsed;
                        currentRate = 1.0 / timeElapsed;
                    }
                    */
                }

                sender.CombLatency = 0;
            }
            /*else
            {
                //Console.WriteLine("THIS IS FAIL: {0} {1}", current.Motion.GyroYaw, current.timeElapsed);
                //Console.WriteLine($"{current.A} {current.B} {current.RShoulder}");
                //Console.WriteLine();
                tempSkipMouse = true;
            }
            */

            if (!tempSkipMouse)
            {
                if (mouseX != 0.0 || mouseY != 0.0)
                {
                    //Console.WriteLine("MOVE: {0}, {1}, {2} {3} {4}", (int)mouseX, (int)mouseY,
                    //    current.timeElapsed, current.Motion.GyroYaw, holdCursorCount);
                    GenerateMouseMoveEvent();
                }
                else
                {
                    // Probably not needed here. Leave as a temporary precaution
                    mouseXRemainder = mouseYRemainder = 0.0;

                    filterX.Filter(0.0, currentRateJoyR); // Smooth on output
                    filterY.Filter(0.0, currentRateJoyR); // Smooth on output
                }
            }

            outputX360.SubmitReport();

            x360ConReportLocker.ExitWriteLock();
        }

        private void Reader_Left_Mixed_Gyro_Report(JoyConReader sender, JoyConDevice device)
        {
            x360ConReportLocker.EnterWriteLock();

            ref JoyConState current = ref device.ClothOff;
            ref JoyConState previous = ref device.ClothOff2;
            //outputX360.ResetReport();
            currentLatency = sender.CombLatency;
            currentRate = 1.0 / currentLatency;

            unchecked
            {
                //ushort tempButtons = 0;
                ushort tempButtons = (ushort)(outputX360.ButtonState & ~leftButtonComb);
                if (current.DpadUp) tempButtons |= Xbox360Button.Up.Value;
                if (current.DpadDown) tempButtons |= Xbox360Button.Down.Value;
                if (current.DpadLeft) tempButtons |= Xbox360Button.Left.Value;
                if (current.DpadRight) tempButtons |= Xbox360Button.Right.Value;
                if (current.Minus) tempButtons |= Xbox360Button.Back.Value;
                //if (current.Capture) tempButtons |= Xbox360Button.Back.Value;
                //if (current.Capture) tempButtons |= Xbox360Button.LeftShoulder.Value;
                //if (current.LSClick) tempButtons |= Xbox360Button.LeftThumb.Value;
                if (current.SR) tempButtons |= Xbox360Button.LeftShoulder.Value;
                //if (current.LShoulder) tempButtons |= Xbox360Button.LeftShoulder.Value;

                disableGyro = current.LShoulder;
                //if (current.SL) tempButtons |= Xbox360Button.LeftShoulder.Value;
                //if (current.SR) tempButtons |= Xbox360Button.RightShoulder.Value;

                outputX360.SetButtonsFull(tempButtons);
            }

            short temp;
            temp = AxisScale(current.LX, JoyConReader.STICK_LX_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_LX, false);
            outputX360.LeftThumbX = temp;

            temp = AxisScale(current.LY, JoyConReader.STICK_LY_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_LY, false);
            outputX360.LeftThumbY = temp;

            //outputX360.LeftTrigger = 0;
            outputX360.LeftTrigger = current.LSClick ? (byte)255 : (byte)0;
            if (current.ZL != previous.ZL)
            {
                mouseRBDown = current.ZL;
                InputMethods.MouseEvent(current.ZL ? InputMethods.MOUSEEVENTF_RIGHTDOWN :
                    InputMethods.MOUSEEVENTF_RIGHTUP);
            }

            outputX360.SubmitReport();
            sender.CombLatency = 0;

            x360ConReportLocker.ExitWriteLock();
        }

        private void Reader_Right_Join_Report(JoyConReader sender, JoyConDevice device)
        {
            x360ConReportLocker.EnterWriteLock();

            ref JoyConState current = ref device.ClothOff;
            ref JoyConState previous = ref device.ClothOff2;
            //outputX360.ResetReport();
            unchecked
            {
                //ushort tempButtons = 0;
                ushort tempButtons = (ushort)(outputX360.ButtonState & ~rightButtonComb);
                if (current.B) tempButtons |= Xbox360Button.A.Value;
                if (current.A) tempButtons |= Xbox360Button.B.Value;
                if (current.Y) tempButtons |= Xbox360Button.X.Value;
                if (current.X) tempButtons |= Xbox360Button.Y.Value;
                if (current.Plus) tempButtons |= Xbox360Button.Start.Value;
                if (current.Home) tempButtons |= Xbox360Button.Guide.Value;
                if (current.RSClick) tempButtons |= Xbox360Button.RightThumb.Value;
                if (current.RShoulder) tempButtons |= Xbox360Button.RightShoulder.Value;
                //if (current.SL) tempButtons |= Xbox360Button.LeftShoulder.Value;
                //if (current.SR) tempButtons |= Xbox360Button.RightShoulder.Value;

                outputX360.SetButtonsFull(tempButtons);
            }

            short temp;
            temp = AxisScale(current.RX, JoyConReader.STICK_RX_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_RX, false);
            //outputX360.LeftThumbX = temp;
            outputX360.RightThumbX = temp;

            temp = AxisScale(current.RY, JoyConReader.STICK_RY_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_RY, false);
            //outputX360.LeftThumbY = temp;
            outputX360.RightThumbY = temp;

            outputX360.RightTrigger = current.ZR ? (byte)255 : (byte)0;

            outputX360.SubmitReport();

            x360ConReportLocker.ExitWriteLock();
        }

        private void Reader_Left_Join_Report(JoyConReader sender, JoyConDevice device)
        {
            x360ConReportLocker.EnterWriteLock();

            ref JoyConState current = ref device.ClothOff;
            ref JoyConState previous = ref device.ClothOff2;
            //outputX360.ResetReport();
            unchecked
            {
                //ushort tempButtons = 0;
                ushort tempButtons = (ushort)(outputX360.ButtonState & ~leftButtonComb);
                /*if (current.DpadUp) tempButtons |= Xbox360Button.Up.Value;
                if (current.DpadDown) tempButtons |= Xbox360Button.Down.Value;
                if (current.DpadLeft) tempButtons |= Xbox360Button.Left.Value;
                if (current.DpadRight) tempButtons |= Xbox360Button.Right.Value;
                */

                // Enforce only two active dirs
                if (current.DpadUp && current.DpadRight) tempButtons |= (ushort)(Xbox360Button.Up.Value | Xbox360Button.Right.Value);
                else if (current.DpadUp && current.DpadLeft) tempButtons |= (ushort)(Xbox360Button.Up.Value | Xbox360Button.Left.Value);
                else if (current.DpadUp) tempButtons |= Xbox360Button.Up.Value;
                else if (current.DpadRight && current.DpadDown) tempButtons |= (ushort)(Xbox360Button.Down.Value | Xbox360Button.Right.Value);
                else if (current.DpadRight) tempButtons |= Xbox360Button.Right.Value;
                else if (current.DpadDown && current.DpadLeft) tempButtons |= (ushort)(Xbox360Button.Down.Value | Xbox360Button.Left.Value);
                else if (current.DpadDown) tempButtons |= Xbox360Button.Down.Value;
                else if (current.DpadLeft) tempButtons |= Xbox360Button.Left.Value;

                if (current.Minus) tempButtons |= Xbox360Button.Back.Value;
                if (current.Capture) tempButtons |= Xbox360Button.Back.Value;
                if (current.LSClick) tempButtons |= Xbox360Button.LeftThumb.Value;
                if (current.LShoulder) tempButtons |= Xbox360Button.LeftShoulder.Value;
                //if (current.SL) tempButtons |= Xbox360Button.LeftShoulder.Value;
                //if (current.SR) tempButtons |= Xbox360Button.RightShoulder.Value;

                outputX360.SetButtonsFull(tempButtons);
            }

            short temp;
            temp = AxisScale(current.LX, JoyConReader.STICK_LX_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_LX, false);
            outputX360.LeftThumbX = temp;

            temp = AxisScale(current.LY, JoyConReader.STICK_LY_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_LY, false);
            outputX360.LeftThumbY = temp;

            outputX360.LeftTrigger = current.ZL ? (byte)255 : (byte)0;

            outputX360.SubmitReport();

            x360ConReportLocker.ExitWriteLock();
        }

        private void Reader_Right_Single_Report(JoyConReader sender, JoyConDevice device)
        {
            ref JoyConState current = ref device.ClothOff;
            ref JoyConState previous = ref device.ClothOff2;
            //outputX360.ResetReport();
            unchecked
            {
                ushort tempButtons = 0;
                if (current.A) tempButtons |= Xbox360Button.A.Value;
                if (current.X) tempButtons |= Xbox360Button.B.Value;
                if (current.B) tempButtons |= Xbox360Button.X.Value;
                if (current.Y) tempButtons |= Xbox360Button.Y.Value;
                if (current.Plus) tempButtons |= Xbox360Button.Start.Value;
                if (current.Home) tempButtons |= Xbox360Button.Guide.Value;
                if (current.RSClick) tempButtons |= Xbox360Button.LeftThumb.Value;
                if (current.SL) tempButtons |= Xbox360Button.LeftShoulder.Value;
                if (current.SR) tempButtons |= Xbox360Button.RightShoulder.Value;

                outputX360.SetButtonsFull(tempButtons);
            }

            short temp;
            // Flip axis value to map correctly to X360 RX
            temp = AxisScale(current.RX, JoyConReader.STICK_RX_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_RX, true);
            //outputX360.LeftThumbX = temp;
            outputX360.LeftThumbY = temp; // Swap axis for single JoyCon

            temp = AxisScale(current.RY, JoyConReader.STICK_RY_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_RY, false);
            //outputX360.LeftThumbY = temp;
            outputX360.LeftThumbX = temp; // Swap axis for single JoyCon

            outputX360.SubmitReport();
        }

        private void Reader_Left_Single_Report(JoyConReader sender, JoyConDevice device)
        {
            ref JoyConState current = ref device.ClothOff;
            ref JoyConState previous = ref device.ClothOff2;
            //outputX360.ResetReport();
            unchecked
            {
                ushort tempButtons = 0;
                //if (current.DpadUp) tempButtons |= Xbox360Button.Up.Value;
                //if (current.DpadDown) tempButtons |= Xbox360Button.Down.Value;
                //if (current.DpadLeft) tempButtons |= Xbox360Button.Left.Value;
                //if (current.DpadRight) tempButtons |= Xbox360Button.Right.Value;
                if (current.DpadUp) tempButtons |= Xbox360Button.X.Value;
                if (current.DpadDown) tempButtons |= Xbox360Button.B.Value;
                if (current.DpadLeft) tempButtons |= Xbox360Button.A.Value;
                if (current.DpadRight) tempButtons |= Xbox360Button.Y.Value;
                if (current.Minus) tempButtons |= Xbox360Button.Start.Value;
                if (current.Capture) tempButtons |= Xbox360Button.Back.Value;
                if (current.LSClick) tempButtons |= Xbox360Button.LeftThumb.Value;
                if (current.SL) tempButtons |= Xbox360Button.LeftShoulder.Value;
                if (current.SR) tempButtons |= Xbox360Button.RightShoulder.Value;

                outputX360.SetButtonsFull(tempButtons);
            }

            short temp;
            temp = AxisScale(current.LX, JoyConReader.STICK_LX_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_LX, false);
            //outputX360.LeftThumbX = temp;
            outputX360.LeftThumbY = temp; // Swap axis for single JoyCon

            // Flip axis value to map correctly to X360 LX
            temp = AxisScale(current.LY, JoyConReader.STICK_LY_MIN, JoyConReader.RECIPROCAL_INPUT_RES_STICK_LY, true);
            //outputX360.LeftThumbY = temp;
            outputX360.LeftThumbX = temp; // Swap axis for single JoyCon

            outputX360.SubmitReport();
        }

        private short AxisScale(int value, int axisMin,
            float reciprocalInputResolution, bool flip)
        {
            unchecked
            {
                float temp = (value - axisMin) * reciprocalInputResolution;
                if (flip) temp = (temp - 0.5f) * -1.0f + 0.5f;
                return (short)(temp * OUTPUT_X360_RESOLUTION + X360_STICK_MIN);
            }
        }

        private void RotateCoordinates(double rotation, int axisX, int axisY, int axisXMin, int axisXMax,
            int axisYMin, int axisYMax, int axisXMid, int axisYMid, out short axisXOut, out short axisYOut)
        {
            double sinAngle = Math.Sin(rotation), cosAngle = Math.Cos(rotation);
            double tempLX = axisX - axisXMid, tempLY = axisY - axisYMid;
            axisXOut = (short)(Clamp(axisXMin, (tempLX * cosAngle - tempLY * sinAngle), axisXMax) + axisXMid);
            axisYOut = (short)(Clamp(axisYMin, (tempLX * sinAngle + tempLY * cosAngle), axisYMax) + axisYMid);
        }

        public static double Clamp(double min, double value, double max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }

        private void PopulateGyroMouseEventData(ref JoyConState current, ref JoyConState previous)
        {
            //const int deadZone = 28; // From Switch Pro
            //const int deadZone = 16;
            const int deadZone = 10;
            //const int deadZone = 0;
            const double GYRO_MOUSE_COEFFICIENT = 0.0095;
            //const double GYRO_MOUSE_OFFSET = 0.8; // Switch Pro
            //const double GYRO_MOUSE_OFFSET = 0.96;
            const double GYRO_MOUSE_OFFSET = 0.85;
            //const double GYRO_MOUSE_OFFSET = 0.0;

            double offset = GYRO_MOUSE_OFFSET;
            double coefficient = GYRO_MOUSE_COEFFICIENT;

            //double timeElapsed = current.timeElapsed;
            double timeElapsed = currentLatencyJoyR;
            // Take possible lag state into account. Main routine will make sure to skip this method
            /*if (previous.timeElapsed <= 0.002)
            {
                timeElapsed += previous.timeElapsed;
                currentRate = 1.0 / timeElapsed;
            }
            */

            // Base speed 5 ms
            double tempDouble = timeElapsed * 3 * 66.67;
            int deltaX = current.Motion.GyroYaw, deltaY = current.Motion.GyroPitch;
            double tempAngle = Math.Atan2(-deltaY, deltaX);
            double normX = Math.Abs(Math.Cos(tempAngle));
            double normY = Math.Abs(Math.Sin(tempAngle));
            int signX = Math.Sign(deltaX);
            int signY = Math.Sign(deltaY);

            int deadzoneX = (int)Math.Abs(normX * deadZone);
            int deadzoneY = (int)Math.Abs(normY * deadZone);

            if (Math.Abs(deltaX) > deadzoneX)
            {
                deltaX -= signX * deadzoneX;
            }
            else
            {
                deltaX = 0;
            }

            if (Math.Abs(deltaY) > deadzoneY)
            {
                deltaY -= signY * deadzoneY;
            }
            else
            {
                deltaY = 0;
            }

            double xMotion = deltaX != 0 ? coefficient * (deltaX * tempDouble)
                + (normX * (offset * signX)) : 0;

            double yMotion = deltaY != 0 ? coefficient * (deltaY * tempDouble)
                + (normY * (offset * signY)) : 0;

            mouseX = xMotion; mouseY = yMotion;
        }

        private void GenerateMouseMoveEvent()
        {
            if (mouseX != 0.0 || mouseY != 0.0)
            {
                if ((mouseX > 0.0 && mouseXRemainder > 0.0) || (mouseX < 0.0 && mouseXRemainder < 0.0))
                {
                    mouseX += mouseXRemainder;
                }
                else
                {
                    mouseXRemainder = 0.0;
                }

                if ((mouseY > 0.0 && mouseYRemainder > 0.0) || (mouseY < 0.0 && mouseYRemainder < 0.0))
                {
                    mouseY += mouseYRemainder;
                }
                else
                {
                    mouseYRemainder = 0.0;
                }

                //mouseX = filterX.Filter(mouseX, 1.0 / 0.016);
                //mouseY = filterY.Filter(mouseY, 1.0 / 0.016);
                mouseX = filterX.Filter(mouseX, currentRateJoyR);
                mouseY = filterY.Filter(mouseY, currentRateJoyR);

                double mouseXTemp = mouseX - (remainderCutoff(mouseX * 1000.0, 1.0) / 1000.0);
                int mouseXInt = (int)(mouseXTemp);
                mouseXRemainder = mouseXTemp - mouseXInt;

                double mouseYTemp = mouseY - (remainderCutoff(mouseY * 1000.0, 1.0) / 1000.0);
                int mouseYInt = (int)(mouseYTemp);
                mouseYRemainder = mouseYTemp - mouseYInt;

                //Console.WriteLine("X ({0}) X({3}) Y ({1}) LAG ({2})", mouseXTemp, mouseYTemp, currentLatencyJoyR, mouseX);
                InputMethods.MoveCursorBy(mouseXInt, mouseYInt);
            }
            else
            {
                mouseXRemainder = mouseYRemainder = 0.0;
                //mouseX = filterX.Filter(0.0, 1.0 / 0.016);
                //mouseY = filterY.Filter(0.0, 1.0 / 0.016);
                filterX.Filter(mouseX, currentRateJoyR);
                filterY.Filter(mouseY, currentRateJoyR);
            }

            mouseX = mouseY = 0.0;
        }

        private double remainderCutoff(double dividend, double divisor)
        {
            return dividend - (divisor * (int)(dividend / divisor));
        }

        private void PrepareGyroMouseJoystickEvent(ref JoyConState current, ref JoyConState previous)
        {
            //const int deadzone = 24; // From Switch Pro
            //const int deadzone = 16;
            const int deadzone = 20;
            const int maxZone = 600;
            const double antidead = 0.54;

            //double timeElapsed = current.timeElapsed;
            double timeElapsed = currentLatencyJoyR;
            // Take possible lag state into account. Main routine will make sure to skip this method
            //if (previous.timeElapsed <= 0.002)
            //{
            //    timeElapsed += previous.timeElapsed;
            //    currentRate = 1.0 / timeElapsed;
            //}

            // Base speed 15 ms
            double tempDouble = timeElapsed * 66.67;
            //Console.WriteLine("Elasped: ({0}) DOUBLE {1}", current.timeElapsed, tempDouble);
            int deltaX = current.Motion.GyroYaw, deltaY = -current.Motion.GyroPitch;
            int maxDirX = deltaX >= 0 ? X360_STICK_MAX : X360_STICK_MIN;
            int maxDirY = deltaY >= 0 ? X360_STICK_MAX : X360_STICK_MIN;
            //int maxDirX = deltaX >= 0 ? 127 : -128;
            //int maxDirY = deltaY >= 0 ? 127 : -128;

            //deltaX = (int)filterX.Filter(deltaX, currentRate); // Smooth on input
            //deltaY = (int)filterY.Filter(deltaY, currentRate); // Smooth on input

            double tempAngle = Math.Atan2(-deltaY, deltaX);
            double normX = Math.Abs(Math.Cos(tempAngle));
            double normY = Math.Abs(Math.Sin(tempAngle));
            int signX = Math.Sign(deltaX);
            int signY = Math.Sign(deltaY);

            int deadzoneX = (int)Math.Abs(normX * deadzone);
            int deadzoneY = (int)Math.Abs(normY * deadzone);

            int maxValX = signX * maxZone;
            int maxValY = signY * maxZone;

            double xratio = 0.0, yratio = 0.0;
            double antiX = antidead * normX;
            double antiY = antidead * normY;

            if (Math.Abs(deltaX) > deadzoneX)
            {
                deltaX -= signX * deadzoneX;
                deltaX = (int)(deltaX * tempDouble);
                deltaX = (deltaX < 0 && deltaX < maxValX) ? maxValX :
                    (deltaX > 0 && deltaX > maxValX) ? maxValX : deltaX;
                //if (deltaX != maxValX) deltaX -= deltaX % (signX * GyroMouseFuzz);
            }
            else
            {
                deltaX = 0;
            }

            if (Math.Abs(deltaY) > deadzoneY)
            {
                deltaY -= signY * deadzoneY;
                deltaY = (int)(deltaY * tempDouble);
                deltaY = (deltaY < 0 && deltaY < maxValY) ? maxValY :
                    (deltaY > 0 && deltaY > maxValY) ? maxValY : deltaY;
                //if (deltaY != maxValY) deltaY -= deltaY % (signY * GyroMouseFuzz);
            }
            else
            {
                deltaY = 0;
            }

            if (deltaX != 0) xratio = deltaX / (double)maxValX;
            if (deltaY != 0) yratio = deltaY / (double)maxValY;

            double xNorm = 0.0, yNorm = 0.0;
            if (xratio != 0.0)
            {
                xNorm = (1.0 - antiX) * xratio + antiX;
            }

            if (yratio != 0.0)
            {
                yNorm = (1.0 - antiY) * yratio + antiY;
            }

            //byte axisXOut = (byte)(xNorm * maxDirX + 128.0);
            //byte axisYOut = (byte)(yNorm * maxDirY + 128.0);
            bool useGyro = false;
            /*if (xNorm != 0.0 || yNorm != 0.0)
            {
                double tempX = (current.RX - STICK_MIN) * reciprocalInputResolution;
                double tempY = (current.RY - STICK_MIN) * reciprocalInputResolution;

                if (xNorm > tempX || yNorm > tempY)
                {
                    useGyro = true;
                }
            }
            */

            useGyro = true;
            if (useGyro)
            {
                double RX = xNorm * maxDirX + 0.0;
                double RY = yNorm * maxDirY + 0.0;

                //outputX360.RightThumbX = (short)RX;
                //outputX360.RightThumbY = (short)RY;
                outputX360.RightThumbX = (short)filterX.Filter(RX, currentRateJoyR); // Smooth on output
                outputX360.RightThumbY = (short)filterY.Filter(RY, currentRateJoyR); // Smooth on output
                //outputX360.RightThumbX = (short)(xNorm * maxDirX + 0.0);
                //outputX360.RightThumbY = (short)(yNorm * maxDirY + 0.0);
                //outputX360.RightThumbX = (short)(xNorm * maxDirX + 0.0);
                //outputX360.RightThumbY = (short)(yNorm * maxDirY + 0.0);
            }
            else
            {
                filterX.Filter(0.0, currentRateJoyR); // Smooth on output
                filterY.Filter(0.0, currentRateJoyR); // Smooth on output

                outputX360.RightThumbX = 0;
                outputX360.RightThumbY = 0;
            }
        }

        private void PrepareButtonResetCombos(DesiredMapping type)
        {
            switch(type)
            {
                case DesiredMapping.Single:
                    // No need to do anything. Break.
                    break;
                case DesiredMapping.Joined:
                    leftButtonComb = (ushort)(Xbox360Button.Up.Value | Xbox360Button.Down.Value | Xbox360Button.Left.Value |
                        Xbox360Button.Right.Value | Xbox360Button.Back.Value | Xbox360Button.LeftThumb.Value |
                        Xbox360Button.LeftShoulder.Value);

                    rightButtonComb = (ushort)(Xbox360Button.A.Value | Xbox360Button.B.Value | Xbox360Button.X.Value |
                        Xbox360Button.Y.Value | Xbox360Button.Start.Value | Xbox360Button.Guide.Value |
                        Xbox360Button.RightThumb.Value | Xbox360Button.RightShoulder.Value);
                    break;
                case DesiredMapping.Mixed_Gyro_Mouse:
                case DesiredMapping.Mixed_Gyro_Mouse_Joystick:
                    leftButtonComb = (ushort)(Xbox360Button.Up.Value | Xbox360Button.Down.Value | Xbox360Button.Left.Value |
                        Xbox360Button.Right.Value | Xbox360Button.Back.Value | Xbox360Button.Back.Value | Xbox360Button.LeftThumb.Value |
                        Xbox360Button.LeftShoulder.Value);

                    rightButtonComb = (ushort)(Xbox360Button.A.Value | Xbox360Button.B.Value | Xbox360Button.X.Value |
                        Xbox360Button.Y.Value | Xbox360Button.Start.Value | Xbox360Button.Guide.Value |
                        Xbox360Button.RightThumb.Value | Xbox360Button.RightShoulder.Value);
                    break;
                default:
                    break;
            }
        }

        public void Stop()
        {
            foreach (JoyConReader reader in deviceReadersMap.Values)
            {
                //reader.StopUpdate();
            }

            deviceReadersMap.Clear();
            joyconDeviceEnumerator.StopControllers();

            outputX360?.Disconnect();
            outputX360 = null;

            vigemTestClient?.Dispose();
            vigemTestClient = null;
        }
    }
}
