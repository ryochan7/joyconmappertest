﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HidLibrary;

namespace joyconmappertest.JoyConLibrary
{
    public class JoyConEnumerator
    {
        public const int NINTENDO_VENDOR_ID = 0x57E;
        public const int JOYCON_L_PRODUCT_ID = 0x2006;
        public const int JOYCON_R_PRODUCT_ID = 0x2007;

        private Dictionary<string, JoyConDevice> foundDevices;

        public JoyConEnumerator()
        {
            foundDevices = new Dictionary<string, JoyConDevice>();
        }

        public void FindControllers()
        {
            IEnumerable<HidDevice> hDevices = HidDevices.Enumerate(NINTENDO_VENDOR_ID,
                JOYCON_L_PRODUCT_ID, JOYCON_R_PRODUCT_ID);
            List<HidDevice> tempList = hDevices.ToList();
            int devCount = tempList.Count();
            string devicePlural = "device" + (devCount == 0 || devCount > 1 ? "s" : "");
            Console.WriteLine("FOUND DEVICES: {0}", devCount);

            foreach (HidDevice hDevice in tempList)
            {
                if (!hDevice.IsOpen)
                {
                    hDevice.OpenDevice(false);
                }

                if (hDevice.IsOpen)
                {
                    //string serial = hDevice.readSerial();
                    //SwitchProDevice tempDev = new SwitchProDevice(hDevice);
                    //foundDevices.Add(hDevice.DevicePath, tempDev);
                    Console.WriteLine("DEVICE OPENED");
                    JoyConDevice tempDev = new JoyConDevice(hDevice);
                    foundDevices.Add(hDevice.DevicePath, tempDev);
                }
            }
        }

        public IEnumerable<JoyConDevice> GetDevices()
        {
            return foundDevices.Values.ToList();
        }

        public void StopControllers()
        {
            foreach (JoyConDevice conDevice in foundDevices.Values)
            {
                conDevice.Detach();
                conDevice.HidDevice.CloseDevice();
                Console.WriteLine("DEVICE CLOSED");
            }

            foundDevices.Clear();
        }
    }
}
