﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HidLibrary;

namespace joyconmappertest.JoyConLibrary
{
    public class JoyConReader
    {
        public const ushort STICK_LX_MAX = 3100;
        public const ushort STICK_LX_MIN = 700;
        public const ushort STICK_LX_RES = STICK_LX_MAX - STICK_LX_MIN;
        public const ushort STICK_LX_NEUTRAL = STICK_LX_MAX - (STICK_LX_RES / 2);
        public const float RECIPROCAL_INPUT_RES_STICK_LX = 1 / (float)STICK_LX_RES;

        public const ushort STICK_LY_MAX = 3300;
        public const ushort STICK_LY_MIN = 1200;
        public const ushort STICK_LY_RES = STICK_LY_MAX - STICK_LY_MIN;
        public const ushort STICK_LY_NEUTRAL = STICK_LY_MAX - (STICK_LY_RES / 2);
        public const float RECIPROCAL_INPUT_RES_STICK_LY = 1 / (float)STICK_LY_RES;

        public const ushort STICK_RX_MAX = 3200;
        public const ushort STICK_RX_MIN = 900;
        public const int STICK_RX_RES = STICK_RX_MAX - STICK_RX_MIN;
        public const ushort STICK_RX_NEUTRAL = STICK_RX_MAX - (STICK_RX_RES / 2);
        public const float RECIPROCAL_INPUT_RES_STICK_RX = 1 / (float)STICK_RX_RES;

        public const ushort STICK_RY_MAX = 3000;
        public const ushort STICK_RY_MIN = 900;
        public const int STICK_RY_RES = STICK_RY_MAX - STICK_RY_MIN;
        public const ushort STICK_RY_NEUTRAL = STICK_RY_MAX - (STICK_RY_RES / 2);
        public const float RECIPROCAL_INPUT_RES_STICK_RY = 1 / (float)STICK_RY_RES;

        private const int IMU_XAXIS_IDX = 0, IMU_YAW_IDX = 0;
        private const int IMU_YAXIS_IDX = 1, IMU_PITCH_IDX = 1;
        private const int IMU_ZAXIS_IDX = 2, IMU_ROLL_IDX = 2;

        private JoyConDevice device;
        //public JoyConDevice Device { get => device; }

        private Thread inputThread;
        private bool activeInputLoop = false;
        private byte[] inputReportBuffer;
        private byte[] outputReportBuffer;
        private byte[] rumbleReportBuffer;

        private double combLatency;
        public double CombLatency { get => combLatency; set => combLatency = value; }

        public delegate void JoyConReportDelegate(JoyConReader sender,
            JoyConDevice device);
        public event JoyConReportDelegate Report;

        public JoyConReader(JoyConDevice device)
        {
            this.device = device;

            inputReportBuffer = new byte[362];
            outputReportBuffer = new byte[device.OutputReportLen];
            rumbleReportBuffer = new byte[JoyConDevice.RUMBLE_REPORT_LEN];
        }

        public void PrepareDevice()
        {
            NativeMethods.HidD_SetNumInputBuffers(device.HidDevice.safeReadHandle.DangerousGetHandle(),
                2);

            device.SetOperational();
        }

        public void StartUpdate()
        {
            PrepareDevice();

            inputThread = new Thread(ReadInput);
            inputThread.IsBackground = true;
            inputThread.Priority = ThreadPriority.AboveNormal;
            inputThread.Name = "JoyCon Reader Thread";
            inputThread.Start();
        }

        public void StopUpdate()
        {
            Console.WriteLine("TERMINATE INPUT LOOP");

            activeInputLoop = false;
            Report = null;
            if (inputThread != null && inputThread.IsAlive)
            {
                //inputThread.Interrupt();
                inputThread.Join();
            }
        }

        private unsafe void ReadInput()
        {
            Console.WriteLine("INPUT LOOP");

            activeInputLoop = true;
            JoyConSide sideType = device.SideType;
            // Keep both stick byte[] for now
            byte[] stick_raw = { 0, 0, 0 };
            byte[] stick_raw2 = { 0, 0, 0 };
            short[] accel_raw = { 0, 0, 0 };
            short[] gyro_raw = new short[9];
            short[] gyro_out = new short[9];
            short tempShort = 0;
            int tempAxis = 0;
            int tempAxisX = 0;
            int tempAxisY = 0;

            long currentTime = 0;
            long previousTime = 0;
            long deltaElapsed = 0;
            double lastElapsed;
            double tempTimeElapsed;
            bool firstReport = true;

            unchecked
            {
                while (activeInputLoop)
                {
                    HidDevice.ReadStatus res = device.HidDevice.ReadWithFileStream(inputReportBuffer);
                    if (res == HidDevice.ReadStatus.Success)
                    {
                        if (inputReportBuffer[0] != 0x30)
                        {
                            Console.WriteLine("Got unexpected input report id 0x{0:X2}. Try again",
                                inputReportBuffer[0]);

                            continue;
                        }
                        else if (firstReport)
                        {
                            Console.WriteLine("CAN READ REPORTS. NICE");
                        }

                        //Console.WriteLine("GOT INPUT REPORT {0} 0x{1:X2}", res, inputReportBuffer[0]);
                        ref JoyConState current = ref device.ClothOff;
                        byte tmpByte;

                        currentTime = Stopwatch.GetTimestamp();
                        deltaElapsed = currentTime - previousTime;
                        lastElapsed = deltaElapsed * (1.0 / Stopwatch.Frequency) * 1000.0;
                        tempTimeElapsed = lastElapsed * .001;
                        combLatency += tempTimeElapsed;

                        if (tempTimeElapsed <= 0.005)
                        {
                            continue;
                        }

                        current.timeElapsed = combLatency;
                        //if (sideType == JoyConSide.Right)
                        //{
                        //    //Console.WriteLine("Poll Time: {0}", lastElapsed);
                        //    if (!firstReport && lastElapsed >= 30.0)
                        //    {
                        //        Console.WriteLine("High Latency: {0} {1}", lastElapsed, DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffffK"));
                        //    }
                        //    if (!firstReport && lastElapsed <= 5.0)
                        //    {
                        //        Console.WriteLine("Low Latency: {0} {1}", lastElapsed, DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffffK"));
                        //    }
                        //}

                        previousTime = currentTime;
                        firstReport = false;
                        combLatency = 0.0;

                        current.FrameTimer = inputReportBuffer[1];

                        tmpByte = inputReportBuffer[2];
                        current.Battery = ((tmpByte & 0xE0) >> 4) * 100 / 8;
                        //current.ConnInfo = (byte)(tmpByte & 0x0F);
                        //current.Charging = (tmpByte & 0x10) != 0;
                        //Console.WriteLine("BATTERY: {0}", current.Battery);
                        //Console.WriteLine("Frame Time: {0}", current.FrameTimer);
                        //Console.WriteLine("CONN INFO: {0}", current.ConnInfo);
                        //Console.WriteLine("Charging: {0}", current.Charging);

                        if (sideType == JoyConSide.Left)
                        {
                            tmpByte = inputReportBuffer[4];
                            current.Minus = (tmpByte & 0x01) != 0;
                            current.LSClick = (tmpByte & 0x08) != 0;
                            current.Capture = (tmpByte & 0x20) != 0;

                            tmpByte = inputReportBuffer[5];
                            current.DpadUp = (tmpByte & 0x02) != 0;
                            current.DpadDown = (tmpByte & 0x01) != 0;
                            current.DpadLeft = (tmpByte & 0x08) != 0;
                            current.DpadRight = (tmpByte & 0x04) != 0;
                            current.LShoulder = (tmpByte & 0x40) != 0;
                            current.ZL = (tmpByte & 0x80) != 0;
                            current.SL = (tmpByte & 0x20) != 0;
                            current.SR = (tmpByte & 0x10) != 0;

                            stick_raw[0] = inputReportBuffer[6];
                            stick_raw[1] = inputReportBuffer[7];
                            stick_raw[2] = inputReportBuffer[8];

                            tempAxisX = (stick_raw[0] | ((stick_raw[1] & 0x0F) << 8));
                            //tempAxisX = tempAxisX > device.leftStickXData.max ? device.leftStickXData.max : (tempAxisX < device.leftStickXData.min ? device.leftStickXData.min : tempAxisX);
                            tempAxisX = tempAxisX > STICK_LX_MAX ? (ushort)STICK_LX_MAX : (tempAxisX < STICK_LX_MIN ? (ushort)STICK_LX_MIN : (ushort)tempAxisX);
                            current.LX = (ushort)tempAxisX;
                            //current.LX = (byte)((tempAxisX - device.leftStickXData.min) / (double)(device.leftStickXData.max - device.leftStickXData.min) * 255);

                            tempAxisY = ((stick_raw[1] >> 4) | (stick_raw[2] << 4));
                            //tempAxisY = tempAxisY > device.leftStickYData.max ? device.leftStickYData.max : (tempAxisY < device.leftStickYData.min ? device.leftStickYData.min : tempAxisY);
                            tempAxisY = tempAxisY > STICK_LY_MAX ? (ushort)STICK_LY_MAX : (tempAxisY < STICK_LY_MIN ? (ushort)STICK_LY_MIN : (ushort)tempAxisY);
                            current.LY = (ushort)tempAxisY;
                            //current.LY = (byte)((((tempAxisY - leftStickYData.min) / (double)(leftStickYData.max - leftStickYData.min) - 0.5) * -1.0 + 0.5) * 255);

                            /*current.LX = (ushort)(stick_raw[0] | ((stick_raw[1] & 0x0F) << 8));
                            current.LX = current.LX > STICK_LX_MAX ? (ushort)STICK_LX_MAX : (current.LX < STICK_LX_MIN ? (ushort)STICK_LX_MIN : current.LX);
                            current.LY = (ushort)((stick_raw[1] >> 4) | (stick_raw[2] << 4));
                            current.LY = current.LY > STICK_LY_MAX ? (ushort)STICK_LY_MAX : (current.LY < STICK_LY_MIN ? (ushort)STICK_LY_MIN : current.LY);
                            */

                            //Console.WriteLine("LX {0}", current.LX);
                            //Console.WriteLine("LY {0}", current.LY);
                        }
                        else if (sideType == JoyConSide.Right)
                        {
                            tmpByte = inputReportBuffer[3];
                            current.A = (tmpByte & 0x08) != 0;
                            current.B = (tmpByte & 0x04) != 0;
                            current.X = (tmpByte & 0x02) != 0;
                            current.Y = (tmpByte & 0x01) != 0;
                            current.RShoulder = (tmpByte & 0x40) != 0;
                            current.ZR = (tmpByte & 0x80) != 0;
                            current.SL = (tmpByte & 0x20) != 0;
                            current.SR = (tmpByte & 0x10) != 0;

                            tmpByte = inputReportBuffer[4];
                            current.Plus = (tmpByte & 0x02) != 0;
                            current.RSClick = (tmpByte & 0x04) != 0;
                            current.Home = (tmpByte & 0x10) != 0;

                            stick_raw2[0] = inputReportBuffer[9];
                            stick_raw2[1] = inputReportBuffer[10];
                            stick_raw2[2] = inputReportBuffer[11];

                            tempAxisX = (stick_raw2[0] | ((stick_raw2[1] & 0x0F) << 8));
                            //tempAxisX = tempAxisX > device.rightStickXData.max ? device.rightStickXData.max : (tempAxisX < device.rightStickXData.min ? device.rightStickXData.min : tempAxisX);
                            tempAxisX = tempAxisX > STICK_RX_MAX ? (ushort)STICK_RX_MAX : (tempAxisX < STICK_RX_MIN ? (ushort)STICK_RX_MIN : (ushort)tempAxisX);
                            current.RX = (ushort)tempAxisX;
                            //current.RX = (byte)((tempAxisX - rightStickXData.min) / (double)(rightStickXData.max - rightStickXData.min) * 255);

                            tempAxisY = ((stick_raw2[1] >> 4) | (stick_raw2[2] << 4));
                            //tempAxisY = tempAxisY > device.rightStickYData.max ? device.rightStickYData.max : (tempAxisY < device.rightStickYData.min ? device.rightStickYData.min : tempAxisY);
                            tempAxisY = tempAxisY > STICK_RY_MAX ? (ushort)STICK_RY_MAX : (tempAxisY < STICK_RY_MIN ? (ushort)STICK_RY_MIN : (ushort)tempAxisY);
                            current.RY = (ushort)tempAxisY;
                            //current.RY = (byte)((((tempAxisY - rightStickYData.min) / (double)(rightStickYData.max - rightStickYData.min) - 0.5) * -1.0 + 0.5) * 255);

                            /*current.RX = (ushort)(stick_raw2[0] | ((stick_raw2[1] & 0x0F) << 8));
                            current.RX = current.RX > STICK_RX_MAX ? (ushort)STICK_RX_MAX : (current.RX < STICK_RX_MIN ? (ushort)STICK_RX_MIN : current.RX);
                            current.RY = (ushort)((stick_raw2[1] >> 4) | (stick_raw2[2] << 4));
                            current.RY = current.RY > STICK_RY_MAX ? (ushort)STICK_RY_MAX : (current.RY < STICK_RY_MIN ? (ushort)STICK_RY_MIN : current.RY);
                            */

                            //Console.WriteLine("RX {0}", current.RX);
                            //Console.WriteLine("RY {0}", current.RY);
                        }

                        for (int i = 0; i < 3; i++)
                        {
                            int data_offset = i * 12;
                            int gyro_offset = i * 3;
                            accel_raw[IMU_XAXIS_IDX] = (short)((ushort)(inputReportBuffer[16 + data_offset] << 8) | inputReportBuffer[15 + data_offset]);
                            accel_raw[IMU_YAXIS_IDX] = (short)((ushort)(inputReportBuffer[14 + data_offset] << 8) | inputReportBuffer[13 + data_offset]);
                            accel_raw[IMU_ZAXIS_IDX] = (short)((ushort)(inputReportBuffer[18 + data_offset] << 8) | inputReportBuffer[17 + data_offset]);

                            tempShort = gyro_raw[IMU_YAW_IDX + gyro_offset] = (short)((ushort)(inputReportBuffer[24 + data_offset] << 8) | inputReportBuffer[23 + data_offset]);
                            //gyro_out[IMU_YAW_IDX + gyro_offset] = (short)(tempShort - device.gyroBias[IMU_YAW_IDX]);
                            gyro_out[IMU_YAW_IDX + gyro_offset] = (short)(tempShort);
                            //gyro_out[IMU_YAW_IDX + gyro_offset] = (short)(tempShort - device.gyroCalibOffsets[IMU_YAW_IDX]);

                            tempShort = gyro_raw[IMU_PITCH_IDX + gyro_offset] = (short)((ushort)(inputReportBuffer[22 + data_offset] << 8) | inputReportBuffer[21 + data_offset]);
                            //gyro_out[IMU_PITCH_IDX + gyro_offset] = (short)(tempShort - device.gyroBias[IMU_PITCH_IDX]);
                            gyro_out[IMU_PITCH_IDX + gyro_offset] = (short)(tempShort);
                            //gyro_out[IMU_PITCH_IDX + gyro_offset] = (short)(tempShort - device.gyroCalibOffsets[IMU_PITCH_IDX]);

                            tempShort = gyro_raw[IMU_ROLL_IDX + gyro_offset] = (short)((ushort)(inputReportBuffer[20 + data_offset] << 8) | inputReportBuffer[19 + data_offset]);
                            //gyro_out[IMU_ROLL_IDX + gyro_offset] = (short)(tempShort - device.gyroBias[IMU_ROLL_IDX]);
                            gyro_out[IMU_ROLL_IDX + gyro_offset] = (short)(tempShort);
                            //gyro_out[IMU_ROLL_IDX + gyro_offset] = (short)(tempShort - device.gyroCalibOffsets[IMU_ROLL_IDX]);

                            //if (sideType == JoyConSide.Left)
                            //{
                            //    Console.WriteLine("LEFT");
                            //}
                            //else if (sideType == JoyConSide.Right)
                            //{
                            //    Console.WriteLine("RIGHT");
                            //}

                            //Console.WriteLine($"IDX: ({i}) Accel: X({accel_raw[IMU_XAXIS_IDX]}) Y({accel_raw[IMU_YAXIS_IDX]}) Z({accel_raw[IMU_ZAXIS_IDX]})");
                            //Console.WriteLine($"IDX: ({i}) Gyro: Yaw({gyro_raw[IMU_YAW_IDX + gyro_offset]}) Pitch({gyro_raw[IMU_PITCH_IDX + gyro_offset]}) Roll({gyro_raw[IMU_ROLL_IDX + gyro_offset]})");
                            //Console.WriteLine($"IDX: ({i}) Gyro OUT: Yaw({gyro_out[IMU_YAW_IDX + gyro_offset]}) Pitch({gyro_out[IMU_PITCH_IDX + gyro_offset]}) Roll({gyro_out[IMU_ROLL_IDX + gyro_offset]})");
                            //Console.WriteLine();

                            //if (sideType == JoyConSide.Right)
                            //{
                            //    Console.WriteLine($"IDX: ({i}) Accel: X({accel_raw[IMU_XAXIS_IDX]}) Y({accel_raw[IMU_YAXIS_IDX]}) Z({accel_raw[IMU_ZAXIS_IDX]})");
                            //    Console.WriteLine($"IDX: ({i}) Gyro: Yaw({gyro_raw[IMU_YAW_IDX + gyro_offset]}) Pitch({gyro_raw[IMU_PITCH_IDX + gyro_offset]}) Roll({gyro_raw[IMU_ROLL_IDX + gyro_offset]})");
                            //    Console.WriteLine($"IDX: ({i}) Gyro OUT: Yaw({gyro_out[IMU_YAW_IDX + gyro_offset]}) Pitch({gyro_out[IMU_PITCH_IDX + gyro_offset]}) Roll({gyro_out[IMU_ROLL_IDX + gyro_offset]})");
                            //    Console.WriteLine();
                            //}
                        }

                        //Console.WriteLine();

                        // For Accel, just use most recent sampled values
                        short accelX = accel_raw[IMU_XAXIS_IDX];
                        short accelY = accel_raw[IMU_YAXIS_IDX];
                        short accelZ = accel_raw[IMU_ZAXIS_IDX];

                        short calibOffsetYaw = device.gyroCalibOffsets[IMU_YAW_IDX],
                            calibOffsetPitch = device.gyroCalibOffsets[IMU_PITCH_IDX],
                            calibOffsetRoll = device.gyroCalibOffsets[IMU_ROLL_IDX];

                        // Just use most recent sample for now
                        short gyroYaw = (short)(-1 * (gyro_out[6 + IMU_YAW_IDX] - device.gyroBias[IMU_YAW_IDX] + calibOffsetYaw));
                        short gyroPitch = (short)(gyro_out[6 + IMU_PITCH_IDX] - device.gyroBias[IMU_PITCH_IDX] - calibOffsetPitch);
                        short gyroRoll = (short)(gyro_out[6 + IMU_ROLL_IDX] - device.gyroBias[IMU_ROLL_IDX] - calibOffsetRoll);
                        //short gyroYaw = (short)(-1 * (gyro_out[6 + IMU_YAW_IDX] - device.gyroBias[IMU_YAW_IDX] - device.gyroCalibOffsets[IMU_YAW_IDX]));
                        //short gyroPitch = (short)(gyro_out[6 + IMU_PITCH_IDX] - device.gyroBias[IMU_PITCH_IDX] + device.gyroCalibOffsets[IMU_PITCH_IDX]);
                        //short gyroRoll = (short)(gyro_out[6 + IMU_ROLL_IDX] - device.gyroBias[IMU_ROLL_IDX] + device.gyroCalibOffsets[IMU_ROLL_IDX]);

                        //short gyroYaw = (short)(-1 * (gyro_out[6 + IMU_YAW_IDX] - device.gyroBias[IMU_YAW_IDX]));
                        //short gyroPitch = (short)(gyro_out[6 + IMU_PITCH_IDX] - device.gyroBias[IMU_PITCH_IDX]);
                        //short gyroRoll = (short)(gyro_out[6 + IMU_ROLL_IDX] - device.gyroBias[IMU_ROLL_IDX]);

                        // JoyCon Right axes are inverted. Adjust axes directions
                        if (sideType == JoyConSide.Right)
                        {
                            accelX *= -1; accelZ *= -1; // accelY *= -1;
                            gyroYaw *= -1; gyroPitch *= -1; gyroRoll *= -1;
                        }
                        current.Motion.Populate(accelX, accelY, accelZ,
                            gyroYaw, gyroPitch, gyroRoll, device.accelCoeff, device.gyroCoeff);
                        //current.Motion.GyroYaw = gyro_out[IMU_YAW_IDX] + gyro_out[3 + IMU_YAW_IDX] + gyro_out[6 + IMU_YAW_IDX];
                        //current.Motion.GyroPitch = gyro_out[IMU_PITCH_IDX] + gyro_out[3 + IMU_PITCH_IDX] + gyro_out[6 + IMU_PITCH_IDX];
                        //current.Motion.GyroRoll = gyro_out[IMU_ROLL_IDX] + gyro_out[3 + IMU_ROLL_IDX] + gyro_out[6 + IMU_ROLL_IDX];

                        /*if (sideType == JoyConSide.Right)
                        {
                            //Console.WriteLine("Final Accel: X({0}), Y({1}), Z({2})",
                            //    current.Motion.AccelX, current.Motion.AccelY, current.Motion.AccelZ);
                            Console.WriteLine("Final Gyro: Yaw({0}), Pitch({1}), Roll({2})",
                                current.Motion.GyroYaw, current.Motion.GyroPitch, current.Motion.GyroRoll);
                        }*/

                        //Console.WriteLine("TEST INPUT STATE: {0}", current.A.ToString());
                        Report?.Invoke(this, device);
                        device.SyncStates();

                        firstReport = false;
                    }
                    else
                    {
                        Console.WriteLine("FAIL");
                        activeInputLoop = false;
                    }

                    //Thread.Sleep(16);
                }
            }

            Console.WriteLine("FINISH INPUT LOOP");
        }

        public void WriteReport()
        {
            if (activeInputLoop)
            {
                //byte[] tmpbuff = new byte[SwitchProDevice.RUMBLE_REPORT_LEN];
                //Array.Clear(rumbleReportBuffer, 0, JoyConDevice.RUMBLE_REPORT_LEN);
                device.PrepareRumbleData(rumbleReportBuffer);
                //Console.WriteLine("RUMBLE BUFF: {0}",
                //    string.Concat(rumbleReportBuffer.Select(i => string.Format("{0:x2} ", i))));
                //bool result = device.HidDevice.WriteOutputReportViaControl(rumbleReportBuffer);
                bool result = device.HidDevice.WriteOutputReportViaInterrupt(rumbleReportBuffer, 100);
                //device.HidDevice.fileStream.Flush();
                //Console.WriteLine("RUMBLE FINISH");
            }
        }
    }
}
